;       Port B definitions
PB_PORT =       0x5005          ; PORTB
PB_ODR  =       PB_PORT+0       ; output
PB_IDR  =       PB_PORT+1       ; input
PB_DDR  =       PB_PORT+2       ; direction
PB_CR1  =       PB_PORT+3       ; control1:
PB_CR2  =       PB_PORT+4       ; control2:

;       Port D definitions
PD_PORT =       0x500F          ; PORTD
PD_ODR  =       PD_PORT+0       ; output
PD_IDR  =       PD_PORT+1       ; input
PD_DDR  =       PD_PORT+2       ; direction
PD_CR1  =       PD_PORT+3       ; control1:
PD_CR2  =       PD_PORT+4       ; control2:

;       Port F definitions
PF_PORT =       0x5019          ; PORTF
PF_ODR  =       PF_PORT+0       ; output
PF_IDR  =       PF_PORT+1       ; input
PF_DDR  =       PF_PORT+2       ; direction
PF_CR1  =       PF_PORT+3       ; control1:
PF_CR2  =       PF_PORT+4       ; control2: