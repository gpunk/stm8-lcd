;--------------------------------------------------------
	.module main
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main

;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area  DATA
_varOne:
	.ds	1	
_lcdchar:
	.ds	1
_lcdcmd:
	.ds	1
_charptr:
	.ds	2

;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED


;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG
__start__stack:
	.ds	1

;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area HOME
__interrupt_vect:
	int s_GSINIT ;reset
	int 0x0000 ;trap
	int 0x0000 ;int0
	int 0x0000 ;int1
	int 0x0000 ;int2
	int 0x0000 ;int3
	int 0x0000 ;int4
	int 0x0000 ;int5
	int 0x0000 ;int6
	int 0x0000 ;int7
	int 0x0000 ;int8
	int 0x0000 ;int9
	int 0x0000 ;int10
	int 0x0000 ;int11
	int 0x0000 ;int12
	int 0x0000 ;int13
	int 0x0000 ;int14
	int 0x0000 ;int15
	int 0x0000 ;int16
	int 0x0000 ;int17
	int 0x0000 ;int18
	int 0x0000 ;int19
	int 0x0000 ;int20
	int 0x0000 ;int21
	int 0x0000 ;int22
	int 0x0000 ;int23
	int 0x0000 ;int24
	int 0x0000 ;int25
	int 0x0000 ;int26
	int 0x0000 ;int27
	int 0x0000 ;int28
	int 0x0000 ;int29
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
__sdcc_gs_init_startup:
__sdcc_init_data:
; stm8_genXINIT() start
	ldw x, #l_DATA
	jreq	00002$
00001$:
	clr (s_DATA - 1, x)
	decw x
	jrne	00001$
00002$:
	ldw	x, #l_INITIALIZER
	jreq	00004$
00003$:
	ld	a, (s_INITIALIZER - 1, x)
	ld	(s_INITIALIZED - 1, x), a
	decw	x
	jrne	00003$
00004$:
; stm8_genXINIT() end
	.area GSFINAL
	jp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME


	.area HOME
__sdcc_program_startup:
	jp	_main
;	return from main will return to caller
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE

	.include "main.h"	

;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:

	
	; init port D
	MOV	PD_DDR, #0b11111111	; set all to output
	MOV	PD_CR1, #0b11111111	; set all with pull-ups
	MOV 	PD_CR2, #0b00000000	; set all at low speed

	; init port B
	MOV 	PB_DDR, #0b01111111	; set b7 to input
	MOV 	PB_CR1, #0b10000000	; set b7 with pull-ups
	MOV 	PB_CR2, #0b01111111	; set all at max speed

	; init port F
	MOV	PF_DDR, #0b00010000	; set f4 to output
	MOV	PF_CR1, #0b00010000 	; set f4 with pull-ups
	MOV	PF_CR2, #0b00010000	; set all at max speed

;			port D connected to lcd
;	b0	BKL
;	b1	RS	hight data, low instruction
;	b2	RW	hight read, low write
;	b3	E	enable/strobe
;	b4	b4
;	b5	b5
;	b6	b6
;	b7	b7	; 7654 3210



; lcd macros ...

	.macro	STRBLCD
	bset	PD_ODR, #3
	bres	PD_ODR, #3
	.endm

	.macro	LCDRDCMD
	bres	PD_ODR, #1	; lcd Command mode
	bset	PD_ODR, #2	; lcd read mode
	.endm

	.macro	CHKBZF
	LCDRDCMD
	bres	PD_DDR, #7	; port D bit 7 , readmode
	bset	PD_ODR, #3	; EN on
	btjt	PD_IDR, #7,.	; lcdbsy;"loop", if portD7 is zero goon
	bres	PD_ODR, #3	; EN off
	bset	PD_DDR, #7	; port D, bit7 , write mode
	.endm

	.macro	LCDWRDATA
	bres	PD_ODR,	#2	; LCD wride mode
	bset	PD_ODR, #1	; LCD data mode
	.endm

	.macro	LCDWRCMD
	bres	PD_ODR,	#2	; LCD wride mode
	bres	PD_ODR, #1	; LCD data mode
	.endm

	call	_lcdsoftinit


; try write H	


	CHKBZF
	mov	PD_ODR, #0b01000011
	STRBLCD
	CHKBZF
	mov	PD_ODR, #0b10000011
	STRBLCD

;try write X

	mov	_lcdchar, #'X'	;
	call	_lcdwritechar


	mov	_varOne, #'a'


main_loop:


	LDW	X, #0d50
	CALL	delay_m
	CALL	ledon


	LDW	X, #0d50
	CALL	delay_m
	CALL	ledoff

	ld	a, #0b00000001	; LCD home/cls
	ld	_lcdcmd, a
	call	_lcdwritecmd

	
	ldw	x,#_string_0	; init pointer
	ldw	_charptr, x
	call	_lcdputstring


	ldw	x,#_string_1	; init pointer
	ldw	_charptr, x
	call	_lcdputstring


	mov	_lcdcmd, #0xC0	; lcd goto line2, 0 
	call	_lcdwritecmd

	ldw	x,#_string_2	; init pointer
	ldw	_charptr, x
	call	_lcdputstring

	
	

	JP	.


_lcdputstring:

	ldw	x, _charptr
	ld	a , (x)
	ld	_lcdchar, a
	tnz	a
	jreq	_lcdputs_end
	call	_lcdwritechar
	ldw	x, _charptr
	incw	x
	ldw	_charptr, x
	jp	_lcdputstring
_lcdputs_end:
	ret			
	


_lcdsoftinit:	; Soft-Init lcd in 4bit mode

	ldw	x, #0d500
	call	delay_m

	mov	PD_ODR, #0b00100001
	STRBLCD
	ldw	x, #0d15
	call	delay_m

	mov	PD_ODR, #0b00100001
	STRBLCD
	ldw	x, #0d15
	call	delay_m
	mov	PD_ODR, #0b11000001
	STRBLCD
	ldw	x, #0d15
	call	delay_m
							
	mov	PD_ODR, #0b00000001
	STRBLCD
	ldw	x, #0d15
	call	delay_m
	mov	PD_ODR, #0b11000001
	STRBLCD
	ldw	x, #0d15
	call	delay_m
							
	mov	PD_ODR, #0b00000001
	STRBLCD
	ldw	x, #0d15
	call	delay_m
	mov	PD_ODR, #0b01100001
	STRBLCD
	ldw	x, #0d15
	call	delay_m

	RET

; end lcd soft-init




_lcdwritecmd:
	
	CHKBZF
	LCDWRCMD

	ld	a,_lcdcmd	; 
	and	a,#0b11110000	; delete lower nib 
	or	a,#0b00000001	; put in that : cmd /  write + bkl ON
	ld	PD_ODR, a	; #0b01000001
	STRBLCD

	ld	a,_lcdcmd	;
	swap	a
	and	a,#0b11110000	; delete lower nib 
	or	a,#0b00000001	; put in that :  cmd / write + bkl ON
	ld	PD_ODR, a
	STRBLCD
	
	ret



_lcdwritechar:
	
	CHKBZF
	LCDWRDATA

	ld	a,_lcdchar	; 
	and	a,#0b11110010	; delete lower nib 
	or	a,#0b00000011	; put in that : data write + bkl ON
	ld	PD_ODR, a	; #0b01000011
	STRBLCD

	ld	a,_lcdchar	;
	swap	a
	and	a,#0b11110010	; delete lower nib 
	or	a,#0b00000011	; put in that : data write + bkl ON
	ld	PD_ODR, a
	STRBLCD
	
	ret


ledon:
	BSET	PF_ODR, #0x04
	ret
ledoff:
	BRES	PF_ODR, #0x04
	ret

delay_m:				; X milli-second delay routine
        LDW     Y, #0x029E              ; Hand-calibrated (approximate) value
m_dloop:
        DECW    Y
        JRNE    m_dloop

        DECW    X
        JRNE    delay_m
        RET


	.area	HOME
_string_0:
        .ascii "Bonj"
        .db 0x00
_string_1:
        .ascii "our!"
        .db 0x00
_string_2:
        .ascii "toi!"
        .db 0x00

	.area INITIALIZER

	.area CABS (ABS)